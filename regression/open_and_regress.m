clc
close all
clear all

% this version: just open and regress (for HARP data)

%% train

train_anew = false;

if train_anew

    % training directory
    training_dir = '/home/dgomez/davidmeows/Tongue_Mechanics/20170104/training_data/';
    % find data
    train_list = dir([training_dir,'run_*.mat']);
    % size(train_list,1)
    
    % collect data
    Y1 = []; X1 = []; % direction 1
    Y2 = []; X2 = []; % direction 2
    for iter = 1:size(train_list,1)
        % load
        load([training_dir, train_list(iter).name]);
        % fill matrices
        Y1 = [Y1;Im_a(:,1)'];
        X1 = [X1;Im_s(:,1)'];
        Y2 = [Y2;Im_a(:,2)'];
        X2 = [X2;Im_s(:,2)'];
    end
    % measure results
    n = size(Y1,1);
    d = size(Y1,2);

    % setup random forrest
    % [X,mu,sigma] = zscore(X);
    B1 = cell(1,d);
    B2 = cell(1,d);
    ntrees = 30;
    options = statset('UseParallel', true);
    parfor iter = 1:d
        iter
        B1{iter} = TreeBagger(ntrees,X1,Y1(:,iter),'method','regression', 'options', options, 'MinLeaf',5, 'OOBVarImp', 'on');
        B2{iter} = TreeBagger(ntrees,X2,Y2(:,iter),'method','regression', 'options', options, 'MinLeaf',5, 'OOBVarImp', 'on');
    end
    save('Tree_2142_complete.mat','B1','B2','n','d');
else
    n = 2142;
    d = 262;
    % load('B1_2142.mat')
    % load('B2_2142.mat') 
end


%% predict
% incoment depending on method

predict_anew = true;

if predict_anew

    
    
    HARP_folder = '/home/dgomez/davidmeows/pg17/tongue_biomech/SPIE_2017_aniket/Paper/HARP_strains';
    HARP_file = 'SIM_strain_5.mat';
    % HARP_file = 'SIM_strain.mat';
    load([HARP_folder '/' HARP_file]) 

    X1test = [Im_s(:,1)'];
    X2test = [Im_s(:,2)'];

    parfor diter = 1:d
        Y1pred(:,diter) = B1{diter}.predict(X1test(:,:));
        Y2pred(:,diter) = B2{diter}.predict(X2test(:,:));
    end

    % predicted activation values
    Pm_lv = [Y1pred' Y2pred'];

    % save values
    save('Prediction_HARP_5','Im_s','Pm_lv')
    % save('Prediction_T1','Im_s','Pm_lv')

end

