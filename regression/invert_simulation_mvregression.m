training_dir = '/home/dgomez/davidmeows/Tongue_Mechanics/20170104/training_data/';

train_list = dir([training_dir,'run_*.mat']);

Y = [];
X = [];
for iter = 1:2040%1:2:64
    load([training_dir, train_list(iter).name]);
    
    % fill up the Y matrix
    
    Y = [Y;Im_a(:,1)'];
    X = [X;Im_s(:,1)',Im_t(:,1)'];
end
n = size(Y,1);
d = size(Y,2);

%[X,mu,sigma]  = zscore(X);
B = cell(1,d);
ntrees = 30;
options = statset('UseParallel', true);
for iter = 1:d
    iter
    B{iter} = TreeBagger(ntrees,X,Y(:,iter),'method','regression', 'options', options, 'MinLeaf',1, 'OOBVarImp', 'on');

end


load([training_dir,'test_ver'])

Xtest = [Im_s(:,1)', Im_t(:,1)'];
Ytest = [Im_a(:,1)'];

Ypred = [];
for iter = 1:d
    iter
    Ypred(:,iter) = B{iter}.predict(Xtest(:,:));
end


% Xmat = [ones(n,1) X];
% 
% B = zeros(size(Xmat,2),d);
% Bl = zeros(size(Xmat,2),d);
% Bu = zeros(size(Xmat,2),d);
% for iter = 1:d
%     [B(:,iter), bint] = regress(Y(:,iter),Xmat);
% end
% 
% 
% 
% Xcell = cell(1,n);
% 
% for i = 1:n
%     Xcell{i} = [kron([Xmat(i,:)],eye(d))];
% end
% 
