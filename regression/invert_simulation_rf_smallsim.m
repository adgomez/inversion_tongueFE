training_dir = '/home/dgomez/davidmeows/Tongue_Mechanics/20170104/training_data/';

train_list = dir([training_dir,'test_*.mat']);

Y1 = [];
X1 = [];

Y2 = [];
X2 = [];
for iter = 1:64
    load([training_dir, train_list(iter).name]);
    
    % fill up the Y matrix
    
    Y1 = [Y1;Im_a(:,1)'];
    X1 = [X1;Im_s(:,1)',Im_t(:,1)'];
    
    Y2 = [Y2;Im_a(:,2)'];
    X2 = [X2;Im_s(:,2)',Im_t(:,2)'];
end
n = size(Y1,1);
d = size(Y1,2);

%[X,mu,sigma]  = zscore(X);
B1 = cell(1,d);
B2 = cell(1,d);
ntrees = 30;
options = statset('UseParallel', true);
parfor iter = 1:d
    iter
    B1{iter} = TreeBagger(ntrees,X1,Y1(:,iter),'method','regression', 'options', options, 'MinLeaf',3, 'OOBVarImp', 'on');
    B2{iter} = TreeBagger(ntrees,X2,Y2(:,iter),'method','regression', 'options', options, 'MinLeaf',3, 'OOBVarImp', 'on');

end

save('RF_primary_smalldataset.mat', 'B1');
save('RF_secondary_smalldataset.mat', 'B2');

% verification
test_list = dir([training_dir,'test_ver_*.mat']);
l2_error_1 = [];
rel_l2_error_1 = [];

l2_error_2 = [];
rel_l2_error_2 = [];

avg_abs_error = [];
rel_avg_abs_error = [];
pred_Im_a = [];
l2_error_primary = [];
rel_l2_error_primary = [];
for iter = 21: 40 %length(test_list)
    iter
    filename = [training_dir, 'test_ver_',num2str(iter),'.mat'];
    load(filename);
%     load([training_dir,test_list(iter).name])
    X1test = [Im_s(:,1)', Im_t(:,1)'];
    Y1test = [Im_a(:,1)'];
    
    X2test = [Im_s(:,2)', Im_t(:,2)'];
    Y2test = [Im_a(:,2)'];
    d = size(Y1test,2);
    for diter = 1:d
        Y1pred(:,diter) = B1{diter}.predict(X1test(:,:));
        Y2pred(:,diter) = B2{diter}.predict(X2test(:,:));
    end
    y1norm = sqrt(sum([Y1test].^2));

    l2_error_primary = [l2_error_primary; sqrt(sum((Y1test - Y1pred).^2))];
    rel_l2_error_primary = [rel_l2_error_primary; sqrt(sum((Y1test - Y1pred).^2))/y1norm];
    l2_error_1 = [l2_error_1; sqrt(sum(([Y1test,Y2test] - [Y1pred,Y2pred]).^2))];
    y1norm = sqrt(sum([Y1test,Y2test].^2));
    rel_l2_error_1 = [rel_l2_error_1; sqrt(sum(([Y1test,Y2test] - [Y1pred,Y2pred]).^2))/y1norm];
    
%     y2norm = sqrt(sum(Y2test.^2));
%     l2_error_2 = [l2_error_2; sqrt(sum((Y2test - Y2pred).^2))];
%     rel_l2_error_2 = [rel_l2_error_2;sqrt(sum((Y2test - Y2pred).^2))/y2norm];
%     
    
%     avg_abs_error = [avg_abs_error;sum(abs(Y1test - Y1pred))/length(find(Y1test~=0))]
%     rel_avg = sum(abs(Y1test - Y1pred)./abs(Y1test))
%     rel_avg_abs_error = [rel_avg_abs_error;rel_avg/length(Y1test)]

    pred_Im_a = [Y1pred', Y2pred'];
    
%     save(['pred_Im_a_',test_list(iter).name], 'pred_Im_a')
%     save(['pred_Im_a_','test_ver_',num2str(iter),'.mat'], 'pred_Im_a')
    
end


% Xmat = [ones(n,1) X];
% 
% B = zeros(size(Xmat,2),d);
% Bl = zeros(size(Xmat,2),d);
% Bu = zeros(size(Xmat,2),d);
% for iter = 1:d
%     [B(:,iter), bint] = regress(Y(:,iter),Xmat);
% end
% 
% 
% 
% Xcell = cell(1,n);
% 
% for i = 1:n
%     Xcell{i} = [kron([Xmat(i,:)],eye(d))];
% end
% 
