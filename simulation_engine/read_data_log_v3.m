function DATA = read_data_log_v3(filename,n_steps,n_data_rows,n_data_columns,n_headerlines,n_step_lines)

% v2: faster read loop

if (nargin == 4)
    n_headerlines = 1;
    n_step_lines = 3;
    disp('WARNING: the default data log file format was applied.')
end

fid = fopen(filename);
    
% read loop
DATA = zeros(n_data_rows,n_data_columns,n_steps);
temp = textscan(fid,'%s',n_headerlines,'Delimiter','\n');
% temp{1}

for step = 1:n_steps
    % tic
    temp = textscan(fid,'%s',n_step_lines,'Delimiter','\n');
    % temp{1}
    temp = textscan(fid,'%f',n_data_rows*n_data_columns,'Delimiter',',');    
    % temp{1}
    DATA(:,:,step) = reshape(temp{1},n_data_columns,n_data_rows)';
    %  clc; disp([num2str(100*step/n_steps) '% complete'])
    % toc
end

fclose(fid);

end