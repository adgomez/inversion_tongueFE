clc, return
close all force
clear all

% latest: including better assessment of previous runs
% this uses last time step only, so it is not as efficient
% TODO: implement random sampling
% TODO: handle failure to converge 

% note this may be easier to visualize if fiber directions are just
% assinged as transverse, and longitudinal

%% load FE model

% geometry file 
FE_folder = pwd;
FE_source_file = 'tongue12_ML_v1.feb';

% change to 1 if files need to be read again 
loadmesh = 1;
if loadmesh
    % get mesh
    mesh_file = [FE_folder '/' FE_source_file];
    [nodes, elements] = feb2mat_v4(mesh_file,'hex8');
    warning('this is a mixed-element model, use element data with caution')
    save([FE_folder '/' 'mesh'],'elements','nodes')
else
    load([FE_folder '/' 'mesh']);
end

n_nodes = size(nodes,1);
n_elem = size(elements,1);

%% load fiber information

matching_folder = '/home/dgomez/davidmeows/Tongue_Mechanics/20160616/mesh_matching';
load([matching_folder '/' 'fiber_info_Atlas'])

% NOTE: f contains fiber orientation for transverse iso. materials in
% elements listed in I_trn. a and d contain primary and secondary info in
% orthotropic materials listed in I_ort, respectivel.

%% sampling schedule
% TODO: add activation limits?

% load curve names
LC_names = {'GG', 'IL', 'SL', 'T ', 'V ', 'SG', 'M'};

N_s      = [ 2     1     1     1     1     1     1   ];
A_min    = [ 0     0     0     0     0     0    -1e-3];
A_max    = [ 0.5   0.5   0.5   0.5   0.5   0.5  -1e-4];

A_ds = (A_max-A_min)./(N_s-1);

a_gg = A_min(1):A_ds(1):A_max(1);
a_il = A_min(2):A_ds(2):A_max(2);
a_sl = A_min(3):A_ds(3):A_max(3);
a_t  = A_min(4):A_ds(4):A_max(4);
a_v  = A_min(5):A_ds(5):A_max(5);
a_sg = A_min(6):A_ds(6):A_max(6);
a_m  = A_min(7):A_ds(7):A_max(7);

% construct finite sampling space
[A_gg,A_il,A_sl,A_t,A_v,A_sg,A_m] = ndgrid(a_gg,a_il,a_sl,a_t,a_v,a_sg,a_m);
LC_space = [A_gg(:) A_il(:) A_sl(:) A_t(:) A_v(:) A_sg(:) A_m(:)];

file_root = 'run';
if exist('LC_space','var')
    save(['training_data' '/' file_root '_solspace'],'LC_space','A_gg','A_il','A_sl','A_t','A_v','A_sg','A_m')
end


%% check for existing files

file_c = 1;
r_ID = [];

max_non = 10; % maximum number of nonexistents
non = 0;

while (0<file_c)
    runFileName = [pwd '/' 'training_data' '/' file_root '_' num2str(file_c) '.mat'];
    if exist(runFileName, 'file')
        load(runFileName)
        r_ID = [r_ID; file_c sp_sample];
        file_c = file_c + 1;
        non = 0;
    else
        if (max_non <= non)
            file_c = 0;
        else
            file_c = file_c + 1;
            non = non + 1;
        end
    end
end

disp(['Found ' num2str(size(r_ID,1)) ' previous files.'])
if (numel(r_ID)>0)
r_ID = r_ID(:,2);
end

%% random sampling
% for requesting samples outside r_ID

sp_random = false;

if sp_random

    % sp_percent = 50; % use these lines to request a percentage
    % sp_N = round(size(LC_space,1)*sp_percent/100); 
    sp_N = 100; % use this to request a set number
    
    % all
    sp_Ip = (1:size(LC_space,1))';
    
    % all but the ones already picked
    sp_Ip(r_ID) = [];
    
    % subset of all minus found
    % sp_cnt = randperm(size(sp_Ip,1),sp_N - size(r_ID,1))' % continue with sampling
    sp_cnt = randperm(size(sp_Ip,1),sp_N)';
    
    % integrate remaining at the end
    sp_cnt = sp_Ip(sp_cnt);

    h = waitbar(0,['Sampling ' num2str(numel(sp_cnt)) ' Configurations out side r_ID' ]);
else
    sp_cnt = (1:size(LC_space,1))';
    h = waitbar(0,['Sampling All ' num2str(numel(sp_cnt)) ' Configurations' ]);
end
      
for cnt_r = (numel(r_ID) + 1):numel(sp_cnt)

%% simulation variables
% TODO: adjust mandible so that it can be expressed as a ratio of maxium
% rotation

% control vars (unused)
% dt = 0.1;
% n_steps = 10; 

% activation fraction at n_steps
LC_f     = [0.00  0.00  0.00  0.00  0.00  0.00  0.00
            0.00  0.00  0.00  0.00  0.00  0.00  0.00];
        
% overwrite for sampling
LC_f(2,:) = LC_space(sp_cnt(cnt_r),:);

%% run smulation

% this variable is for future implementation.
ver_n = 2;

% file names 
file_a = [FE_source_file(1:end-5) num2str(ver_n) '_a.txt']; % control
file_b = [FE_source_file(1:end-5) num2str(ver_n) '_b.txt']; % mat/geo/bc
file_c = [FE_source_file(1:end-5) num2str(ver_n) '_c.txt']; % loads
file_d = [FE_source_file(1:end-5) num2str(ver_n) '_d.txt']; % output

% sim file
file_FE   = [FE_source_file(1:end-5) num2str(ver_n) '.feb'];
file_coor = [FE_source_file(1:end-5) num2str(ver_n) '_coor.txt'];
file_ls   = [FE_source_file(1:end-5) num2str(ver_n) '_ls.txt'];
file_lt   = [FE_source_file(1:end-5) num2str(ver_n) '_lt.txt'];
file_vec  = [FE_source_file(1:end-5) num2str(ver_n) '_vec.txt'];

% *** export load curves orientation to FEBIO
% fileID = fopen([FE_folder '/' 'load_data.txt'],'w');
fileID = fopen([FE_folder '/' file_c],'w');
formatSpec = '\t<LoadData>\n';
fprintf(fileID,formatSpec);

for cnt_m = 1:size(LC_f,2)
    formatSpec = ['\t\t<loadcurve id="%3i" name="' LC_names{cnt_m} '" type="linear"> \n'];
    fprintf(fileID,formatSpec,cnt_m);
        formatSpec = '\t\t\t<loadpoint>0.0,%.3f</loadpoint>\n';
        fprintf(fileID,formatSpec,LC_f(1,cnt_m));
        formatSpec = '\t\t\t<loadpoint>1.0,%.3f</loadpoint>\n';
        fprintf(fileID,formatSpec,LC_f(2,cnt_m));            
    formatSpec = '\t\t</loadcurve>\n';
    fprintf(fileID,formatSpec);
end
return
formatSpec = '\t</LoadData>\n';
fprintf(fileID,formatSpec);
fclose(fileID);

% form harp fe files for this time point
% (UNIX)
system(['cat ' file_a ' ' file_b ' ' file_c ' ' file_d ' > ' FE_folder '/' file_FE]);
% (WIN7) -- check
% system(['copy /b '  file_a '+' file_b '+' file_c  '+' file_d  ' ' file_FE]);

% execute FEBio
% (UNIX)
% system(['./febio2.lnx64 -i ' file_FE ]); 
system(['unset LD_LIBRARY_PATH; ' './febio2.lnx64 -i ' file_FE ]);
% (WIN7)
% system(['FEBio2 -i ' file_FE ]); 
    
% find number of steps completed
file_log = [file_FE(1:end-4) '.log'];
fid = fopen(file_log);
temp = textscan(fid,'%s',1,'Delimiter','\n');
while isempty(strfind(cell2mat(temp{1}),'Number of time steps completed'))
    temp = textscan(fid,'%s',1,'Delimiter','\n');  
end
temp = cell2mat(temp{1});   
n_steps = str2num(temp(54:end));
fclose(fid);  

if (n_steps > 0)

%% read simulation output
% NOTE: read array size is different

% load nodal displacements
Nlist = read_data_log_v3(file_coor,n_steps,n_nodes,4,0,4);

% local strain
Slist = read_data_log_v3(file_ls,n_steps,1975,7,0,4);

% local stress
Tlist = read_data_log_v3(file_lt,n_steps,1975,7,0,4);

% fiber direction
% Vlist = read_data_log_v3(file_vec,n_steps,1975,7,0,4);

%% get vars of interest

% material association: mat#, lc_dir1, lc_dir2
M_asn = [1 0 0 
         2 1 0
         3 1 3
         4 1 4
         5 2 4
         6 3 5
         7 4 5
         8 6 0];
     
% lets write the loadcurves

% material ID
Im = elements(1:n_elem,2);

% load curve per element (zero means none)
Im_lc = [M_asn(Im,2) M_asn(Im,3)]; % dir 2

% activation per element
Im_a = zeros([size(Im_lc)]);
Im_a(Im_lc(:,1)>0,1) = LC_f(2,Im_lc(Im_lc(:,1)>0,1));
Im_a(Im_lc(:,2)>0,2) = LC_f(2,Im_lc(Im_lc(:,2)>0,2));

% local strain 
Im_s = [Slist(1:n_elem,2,n_steps) Slist(1:n_elem,3,n_steps)];

%% active stress (blind) approximation

% active contraction variables
Tmax = 35;
ca0 = 4.35;
camax = 4.35;
beta = 4.75;
l0 = 1.58;
refl = 2.04;

% approximate fiber stretch
lamd = (2*Im_s + 1).^0.5;

% current sarcomere length
strl = refl.*lamd;

% sarcomere length change
dl = strl - l0;

% calcium sensitivity
eca50i = (exp(beta*dl) - 1);

% ratio of Camax/Ca0
rca = camax/ca0;

% active fiber stress
Im_as = (Tmax.*(eca50i ./ ( eca50i + rca^2 )));

% remove changes beyond minimum length
Ip = (dl >= 0);  
Im_as(~Ip) = 0;

% scaled stress (requires knowing activation)
Im_asc = Im_as.*Im_a;
  
%% total stress

% local stress 
Im_t = [Tlist(1:n_elem,2,n_steps) Tlist(1:n_elem,3,n_steps)];

%% nodal displacements 
% NOTE : only the ones associated with the hex8

I_nn = unique(elements(1:n_elem,3:10));

In_r = nodes(I_nn,2:4);
In_n = Nlist(I_nn,2:4,n_steps);
In_u = In_n - In_r;


%% storage

% [e1_i e2_i]
% Im_a = activation 
% Im_s = fiber strain 
% Im_t = total fiber stress
% Im_as = unscaled active stress

% [xi yi zi]
% In_u = nodal displacement

% [mj]
% LC_f
% LC_names

% scalar
% sp_sample

sp_sample = sp_cnt(cnt_r);

save(['training_data' '/' file_root '_ver_' num2str(cnt_r) ],'Im_a','Im_s','Im_t','Im_as','In_u', ...
                                                  'sp_sample', ...
                                                  'LC_f','LC_names')

end                                              
                                                                                         
waitbar(cnt_r/numel(sp_cnt),h)

end

close(h)

return



%% plots

% plot
figure
plot(Im_a(:),Im_s(:),'.')
xlabel('activation')
ylabel('fiber strain')

% plot
figure
plot(Im_a(:),Im_t(:),'.')
xlabel('activation')
ylabel('total stress')
     
% plot
figure
plot(Im_a(:),Im_asc(:),'.')
xlabel('activation')
ylabel('scaled active stress')
