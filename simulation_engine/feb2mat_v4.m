function [nodes, elements] = feb2mat_v4(filename,el_token)

% NOTE: only supports 9 materials
% NOTE 2: no BC or other stuff is read, use Preview for that.

%% read FEBIO file with mesh definition

% filename = 'blocko_v4.feb';

str = fileread(filename);

% extract nodal coordinates
expr = '<(\Nodes).*?>.*?</\1>';
[node_str] = regexp(str, expr,'match');
node_str = node_str{1};

tok_name = 'node';
expr = ['<(' tok_name ').*?>.*?</\1>'];
[mstr] = regexp(node_str, expr,'match');

dim = size(mstr);
for cnt_n = 1:dim(2)
    temp = mstr{cnt_n};
    node_str = temp(length(tok_name) + length(num2str(cnt_n)) + 9:end - length(tok_name) - 3);
    nodes(cnt_n,:) = str2num(node_str);
end

nodes = [(1:length(nodes))' nodes];

% extract element definitions
expr = '<(\Elements).*?>.*?</\1>';
[df_str] = regexp(str, expr,'match');
df_str = df_str{1};

tok_name = el_token;
expr = ['<(' tok_name ').*?>.*?</\1>'];
[mstr] = regexp(df_str, expr,'match');

dim = size(mstr);
for cnt_n = 1:dim(2)
    temp = mstr{cnt_n};
    mat(cnt_n,:) = str2num(temp(length(tok_name) + length(num2str(cnt_n)) + 14));
    vect_str = temp(length(tok_name) + length(num2str(cnt_n)) + 18:end - length(tok_name) - 3);
    elements(cnt_n,:) = str2num(vect_str);
end
elements = [(1:length(elements(:,1)))' mat elements];

% size(elements)
% size(nodes)
% uses preview format

end