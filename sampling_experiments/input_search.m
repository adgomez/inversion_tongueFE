clc
close all
clear all

% latest: demo. no PCA to keep things simple

%% toy simulation

% create a mesh
Nm = 5; % 16 = real, 13 = downsample
mesh = ones(Nm);

% set contraction steps
Nc = 2; % 5 = real
ct = linspace(0,1,Nc)';

% define solution space for single contraction (null is omitted)
% pb = de2bi((1:(2^numel(mesh)-1))');

% define solution space for all contractions
% p = repmat(pb,[1,1,numel(ct)]).*repmat(permute(ct,[3 2 1]),[size(pb) 1]);
% p = reshape(permute(p,[1 3 2]),[size(p,1)*size(ct,1) numel(mesh)]);

% include null (if needed)
% p = [p; p(1,:)*0];

%% low-order model

% TIP: use bi2de as unique id
% po = randi([0 1], 1, numel(mesh));
po = ct(randi(numel(ct), 1, numel(mesh)))';

% test image
IM = reshape(po,size(mesh));

% some geometry
x1 = linspace(-0.5,0.5,size(mesh,1));
x2 = linspace(-0.5,0.5,size(mesh,2));
[X1,X2] = ndgrid(x1,x2);
X3 = X1*0;

% mask
R = (X1.^2 + X2.^2).^0.5;
msk = R < 0.45; % 0.45

% mesh (its 3D, so have to downgrade)
IMm = cat(3,IM,IM)*0+1;
[nodes,elements,materials] = pix2mesh_v1(IMm);
map(nodes(1:2:end,1)) = 1:numel(nodes(1:2:end,1));
map = map';
nodes = [map(nodes(1:2:end,1)) nodes(1:2:end,2:end)];
elements = [elements(:,1) materials map(elements(:,2:5))];

% initialize global matrix
A = eye(numel(mesh))*0;

% assemble based on made-up local function
for cnt_n = 1:size(nodes,1)

    % distance  
    Dn = ( sum((nodes(:,2:4) - nodes(cnt_n,2:4)).^2,2).^0.5 )';
    
    % gaussian mask
    sig = 1.1; % 10.1 = lots 0.1 = basically no blurr
    mu = 0;
    Wn = gaussmf(Dn,[sig mu]);
  
    % mask size
    ds = 20; % 20

    % local function
    In = find(Dn <= ds);
    Sn = Wn(In);
    Sn = Sn/norm(Sn); 
    
    % assembly
    A(cnt_n,In) = A(cnt_n,In) + Sn;
    % A(In,cnt_n) = A(In,cnt_n) + Sn';
end

% combine with mask
% B = A*diag(msk(:));

% no mask
% B = A;

% mask only
B = diag(msk(:));

% apply
po = B*IM(:);
im = reshape(po,size(mesh));

% 1D
kd = randi([0 1],1,numel(mesh));
% D = @(v)((B*v')*norm(v)/norm(B*v'))'; % equalized -- more image-like
% D = @(v)((B*v')/norm(B*v'))'; % normalized -- more difussion-like
D = @(v)(B*v')'; % additive -- more force-like
% D = @(v)double((B*v')>0)'; % binary

% for comparison look at built-in function
% k = fspecial( 'gaussian', numel(x1), sig);
% D = @(v)reshape(imfilter(reshape(v,size(mesh)).*msk,k),[1 numel(mesh)]);

% 2D (TBA)
ku = randi([0 1],1,numel(mesh));
kv = randi([0 1],1,numel(mesh));
U = @(v)v.*ku;
V = @(v)v.*kv;

%% test of random input

if false
    figure
    subplot(141)
    imshow(IM,[],'InitialMagnification','fit')
    title('input')
    subplot(142)
    imshow(msk,[],'InitialMagnification','fit')
    title('mask')
    subplot(143)
    Ic = sub2ind(size(mesh),round(0.5*size(mesh,1)),round(0.5*size(mesh,2)));
    IM1 = mesh*0; IM1(Ic:Ic) = 1;
    im1 = reshape(A*IM1(:),size(mesh));
    imshow(im1,[],'InitialMagnification','fit')
    title('PSF')
    subplot(144)
    imshow(im,[],'InitialMagnification','fit')
    title('output')
    return
end

%% error sensitivity tunning

if true
    
    Ic = sub2ind(size(mesh),round(0.5*size(mesh,1)),round(0.5*size(mesh,2)));
    
    if false
        IM1 = mesh*0; IM1(Ic:Ic) = 1;
        IM2 = mesh*0; IM2(Ic-1:Ic-1) = 1;
    else
        IM1 = reshape(ct(randi(numel(ct), 1, numel(mesh)))',size(mesh));
        IM2 = IM1; IM2(Ic-5:Ic) = IM1(Ic:Ic+5);
    end
    
    im1 = reshape(D(IM1(:)'),size(mesh));
    im2 = reshape(D(IM2(:)'),size(mesh));
    
    % size of a small response (associated with n_tol)
    magnitude_1 = norm(im1)
    magnitude_2 = norm(im2)
    % size of difference (associated with m_tol)
    difference = norm(im1 - im2)

    
    figure
    subplot(221)
    imshow(IM1,[0 max(IM1(:))],'InitialMagnification','fit')
    title('input1')
    subplot(222)
    imshow(IM2,[0 max(IM1(:))],'InitialMagnification','fit')
    title('input2')
    subplot(223)
    imshow(im1,[],'InitialMagnification','fit')
    % imshow(im1,[0 max(im1(:))],'InitialMagnification','fit')
    title('output1')
    subplot(224)
    imshow(im2,[],'InitialMagnification','fit')
    % imshow(im2,[0 max(im1(:))],'InitialMagnification','fit')
    title('output2')
    return
end

%% random sampling

% number of possible samples
Ns = Nc^numel(mesh)

% initialize pattern
B_p = IM(:)';

% load test_basis 
% B_p = B_i;

% initialize ok counter
cnt_g = 0;

tic

max_iter = 1e4;
% Le = zeros(max_iter,1);
% Me = zeros(max_iter,1);
% Se = zeros(max_iter,1);
% Te = zeros(max_iter,1);
% B_p = zeros(max_iter,numel(mesh));
    
for cnt_i = 1:max_iter
    
    % low-order model in new basis
    Dp = B_p*0;
    for cnt_b = 1:size(B_p,1)
        % Dp(cnt_b,:) = D(ct( round(B_p(cnt_b,:)*(numel(ct)-1)) + 1 )'); % quantize to ct via lookup table
        Dp(cnt_b,:) = D(B_p(cnt_b,:)); % average
    end
    Up = U(B_p);
    Vp = V(B_p);
        
    % random sample
    % cnt_p = randperm(size(p,1),1);
    % cnt_p = cnt_i;
    % p_i = p(cnt_p,:);
    
    % p_i = randi([0 1], 1, numel(mesh));
    p_i = ct(randi(numel(ct), 1, numel(mesh)))';
    
    % perform low-order sim
    Di = D(p_i); 
    Ui = U(p_i);
    Vi = V(p_i);

    % define residual magitude
    % Mrr = sum((Up - Ui).^2,2).^0.5 + sum((Vp - Vi).^2,2).^0.5;
    % Mr = min(Mrr);
    
    mrr = sum((Dp - Di).^2,2).^0.5;
    [mr,mi] = min(mrr);
    
    % tolerance definition
    m_tol = 0.5; %0.25; % error above this will be added to basis
    n_tol = 0.5; % minimum magnitude to be considered nontrivial
    
    % test
    Le(cnt_i,1) =  mr*(n_tol<norm(Di));
    Me(cnt_i,1) =  mean(Le(1:cnt_i));
    Se(cnt_i,1) =  std(Le(1:cnt_i));
    Te(cnt_i,1) =  mean(Se(1:cnt_i));
    
    if (m_tol < Le(cnt_i,1))&&(n_tol<norm(Di)) % last statement: is it needed here? 
        % add pattern
        B_p = [B_p; p_i];
        % reset ok projection 
        cnt_g = 0;
    else
        % average closest
        B_p(mi,:) = mean([B_p(mi,:); p_i]);
        % add to the ok conunter
        cnt_g = cnt_g + 1;
    end
    
%     % terminate via std convergence
%     e_tol = 0.01;
%     Ts = abs(std(Le(1:cnt_i)) - mean(Se(1:cnt_i)))/mean(Se(1:cnt_i));
%     if (Ts < e_tol)&&(0.1*max_iter < cnt_i)
%         disp(['error converged at: ' num2str(mean(Le(1:cnt_i)))])
%         break
%     end
%     
%     %  terminate by number of successes
%     g_tol = 0.1*max_iter;
%     if (g_tol <= cnt_g)
%         disp(['maximum acceptable candidates: ' num2str(cnt_g)])
%         break
%     end
    
    % terminate via iterations
    if (cnt_i == max_iter)
        disp('reached maximum iterations')
    end
end

toc

figure
hold on
plot(Le)
plot(Me)
plot(Se)
plot(Te)
plot(Le*0 + m_tol,'--k')
grid on
legend('e','e_{avg}','e_{std}','mean e_{std}','m_tol')
xlabel('iteration')

Basis_DOF = size(B_p,1)
