clc
close all
clear all

% latest: basic 2D example
% TODO: use 3D definition for tongue anatomy
% another idea is to use wavelet coefficients of a given distribution
% keywords for future use: entropy, structural similarity, products of experts

% % create a mesh (this will be used in future versions)
% Nm = 16; % 16 = real, 13 = downsample
% mesh = ones(Nm);

% set contraction steps
Nc = 5; % 5 = real
ct = linspace(0,1,Nc)';

%% low-order model
% NOTE candefine individual if needed.

% get random image
% po = ct(randi(numel(ct), 1, numel(mesh)))';
% IM = reshape(po,size(mesh));

% camera man
X = double(imread('cameraman.tif'));
nn = 4;
IM = X(1:nn:end,1:nn:end);
IM = IM./max(IM(:));

% circle
[X1,X2] = ndgrid(linspace(-1,1,size(IM,1)), ...
                linspace(-1,1,size(IM,2)));
           
IM = double((X1.^2 + X2.^2).^0.5 < 0.75);

%% wavelet generation

% TODO: determine a good depth n = fix(log2(size(IM,1)));
% apply transform
w_level = 3;
w_name = 'sym8'; %'haar', 'sym4', 'db2'
[WT,ST] = wavedec2(IM,w_level,w_name);

[H1,V1,D1] = detcoef2('all',WT,ST,1);
[H2,V2,D2] = detcoef2('all',WT,ST,2);
[H3,V3,D3] = detcoef2('all',WT,ST,3);
A3 = appcoef2(WT,ST,w_name,3);

% find and reduce coeff close to zero
WT_alt = [A3(:); H3(:); V3(:); D3(:); ...
                 H2(:); V2(:); D2(:); ...
                 H1(:); V1(:); D1(:)]';
  
Ip = abs(WT_alt) < mean(abs(WT_alt));
WT_alt(Ip) = 0;

% define ID
WT_id = [A3(:)*0+1; H3(:)*0+2;  V3(:)*0+3;  D3(:)*0+4; ...
                    H2(:)*0+5;  V2(:)*0+6;  D2(:)*0+7; ...
                    H1(:)*0+8;  V1(:)*0+9;  D1(:)*0+10]';

% alter additional
In = ~Ip;

% Gaussian model
mu = WT_alt(In)';
sigma = 0*mu; %  initialization

% this determines the level of deformation
I1 = WT_id(In) == 1;
I2 = (2 <= WT_id(In))&(WT_id(In) < 5);
I3 = (5 <= WT_id(In))&(WT_id(In) < 8);
I4 = (8 <= WT_id(In))&(WT_id(In) < 10);

sigma(I1) = 2.5*mu(I1);
sigma(I2) = 1.5*mu(I2);
sigma(I3) = 0;
sigma(I4) = 0;

WT_alt(In) = mu + sigma.*randn(size(mu));

%  reconstruct
wm = waverec2(WT_alt,ST,w_name);

%% quantize

IM(IM<min(ct)) = min(ct);
IM(max(ct)<IM) = max(ct);
IM = ct(round(IM*(numel(ct)-1)) + 1 ); % quantize to ct via lookup table
   
wm(wm<min(ct)) = min(ct);
wm(max(ct)<wm) = max(ct);
im = ct(round(wm*(numel(ct)-1)) + 1 ); % quantize to ct via lookup table
   
%% plot

figure
subplot(121)
imshow(IM,[],'InitialMagnification','fit')
title('init')
subplot(122)
imshow(im,[],'InitialMagnification','fit')
title('reco')


